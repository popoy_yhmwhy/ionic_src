import { Component, OnInit, ViewChild } from '@angular/core';
import { SoccerschoolService } from '../../api/soccerschool.service';
import { LoadingController } from '@ionic/angular';
import { Router,ActivatedRoute} from '@angular/router';



declare var google;


@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.page.html',
  styleUrls: ['./program-detail.page.scss'],
})
export class ProgramDetailPage implements OnInit {
  map;
  data;
  detail;
  params;

  @ViewChild('mapElement') mapElement;
  constructor(
    public soccerschoolService : SoccerschoolService,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.loadDetail();
  }

  async loadDetail(){

    const loading =  await this.loadingController.create({
      message: 'Loading Registration Detail...',
    }).then(res => {
      res.present();
      this.route.params.subscribe( params => {
        this.params = params;
        this.soccerschoolService.registrationDetail(this.params.id).subscribe(data => {
          this.data = data;
          this.detail = this.data.registration;
          res.dismiss();
        });
    });
  


      
    });

   



    

  }


  attend(){
    this.soccerschoolService.attending(1,this.params.id).subscribe(data => {
      this.loadDetail();
    });
  }

  cancel(){
    this.soccerschoolService.attending('null',this.params.id).subscribe(data => {
      this.loadDetail();
    });
  }

  disregard(){
    this.soccerschoolService.attending(0,this.params.id).subscribe(data => {
      this.loadDetail();
    });
  }





  /*
  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    this.map = new google.maps.Map(
      this.mapElement.nativeElement,{
        center: { lat: -34.397, lng: 150.644 },
        zoom:8
      }
    )
    
  } */

}
