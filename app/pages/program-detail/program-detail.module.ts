import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProgramDetailPage } from './program-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProgramDetailPage
  },
  { path: 'makeup-class', loadChildren: '../../pages/makeup-class/makeup-class.module#MakeupClassPageModule' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProgramDetailPage]
})
export class ProgramDetailPageModule {}
