import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SoccerschoolService } from '../../api/soccerschool.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm;
  email:string;
  password:string;
  data;
 

  constructor(
    private router: Router,
    public soccerschoolService : SoccerschoolService,
    public loadingController: LoadingController,
    public toastController: ToastController
   
  ) { 

   


  }


  
  ngOnInit() {
   this.soccerschoolService.checkToken().subscribe(data => {
     console.log(data);
   });
  }
  
  async presentToast() {
    const toast = await this.toastController.create({
      color: 'danger',
      position: 'top',
      message: 'Authentication Failed',
      duration: 2000
    });
    toast.present();
  }


  async login(){
    const loading =  await this.loadingController.create({
      message: 'Authenticating...',
    }).then(res => {
      res.present();
      this.soccerschoolService.login(this.email,this.password).subscribe(data => {
        this.data = data;
        if(this.data.detail.length > 0){
          localStorage.setItem("user",JSON.stringify(this.data.detail[0]));
          localStorage.setItem("token",this.data.detail[0].token);
          this.router.navigate(['']);
        }else{
          this.presentToast();
        }
        res.dismiss();
     });
    });

  }




}
