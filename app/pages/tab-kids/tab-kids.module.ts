import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabKidsPage } from './tab-kids.page';

const routes: Routes = [
  {
    path: '',
    component: TabKidsPage
  },
  { path: 'user-detail/:id', loadChildren: '../../pages/user-detail/user-detail.module#UserDetailPageModule' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabKidsPage]
})
export class TabKidsPageModule {}
