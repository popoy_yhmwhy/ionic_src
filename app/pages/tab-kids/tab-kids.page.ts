import { Component, OnInit } from '@angular/core';
import { SoccerschoolService } from '../../api/soccerschool.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-tab-kids',
  templateUrl: './tab-kids.page.html',
  styleUrls: ['./tab-kids.page.scss'],
})
export class TabKidsPage implements OnInit {

  kids:any;
  constructor(
    public soccerschoolService : SoccerschoolService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.loadKids();
  }


  

  async loadKids(){
    const loading =  await this.loadingController.create({
      message: 'Loading Kids...',
    }).then(res => {
      res.present();
      this.soccerschoolService.kids().subscribe(data => {
        this.kids = data;
        res.dismiss();
      });
    });

  }
  

}
