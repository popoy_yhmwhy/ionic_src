import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabKidsPage } from './tab-kids.page';

describe('TabKidsPage', () => {
  let component: TabKidsPage;
  let fixture: ComponentFixture<TabKidsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabKidsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabKidsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
