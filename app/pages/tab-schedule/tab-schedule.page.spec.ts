import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabSchedulePage } from './tab-schedule.page';

describe('TabSchedulePage', () => {
  let component: TabSchedulePage;
  let fixture: ComponentFixture<TabSchedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabSchedulePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabSchedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
