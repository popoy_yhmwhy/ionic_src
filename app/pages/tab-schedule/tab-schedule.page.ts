import { Component, OnInit } from '@angular/core';
import { SoccerschoolService } from '../../api/soccerschool.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab-schedule',
  templateUrl: './tab-schedule.page.html',
  styleUrls: ['./tab-schedule.page.scss'],
})
export class TabSchedulePage implements OnInit {
  classType: any = 'new';
  upcomingSchedule:any;
  makeUpSchedule:any;
  positionTop:boolean = false;


  constructor( 
    public soccerschoolService : SoccerschoolService,
    public loadingController: LoadingController
    ) { }

  ngOnInit() {
    this.loadUpcomingSchedule();
    this.loadMakeupClass();
     
  }

  ionViewDidEnter(){
  
  }
  
  
  logScrolling($event){
    if($event.detail.currentY == 0){
        this.positionTop = false;
    }else{
        this.positionTop = true;
    }
      
  }




  async loadUpcomingSchedule(){
    const loading =  await this.loadingController.create({
      message: 'Loading Upcoming Schedule...',
    }).then(res => {
      res.present();
      this.soccerschoolService.upcomingSchedule().subscribe(data =>{
        this.upcomingSchedule = data;
        res.dismiss();
      });
    });
  }

  loadMakeupClass(){
    
  }

  


}
