import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabSchedulePage } from './tab-schedule.page';

const routes: Routes = [
  {
    path: '',component: TabSchedulePage
  },
  { path: 'program-detail/:id', loadChildren: '../../pages/program-detail/program-detail.module#ProgramDetailPageModule' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabSchedulePage]
})
export class TabSchedulePageModule {}
