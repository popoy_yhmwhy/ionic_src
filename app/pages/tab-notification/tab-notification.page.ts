import { Component, OnInit } from '@angular/core';
import { SoccerschoolService } from '../../api/soccerschool.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-tab-notification',
  templateUrl: './tab-notification.page.html',
  styleUrls: ['./tab-notification.page.scss'],
})
export class TabNotificationPage implements OnInit {
  notifications:any;

  constructor( 
    public soccerschoolService : SoccerschoolService,
    public loadingController: LoadingController
    ) { }

  ngOnInit() {
    this.loadNotification();
  }

  async loadNotification(){
    

    const loading =  await this.loadingController.create({
      message: 'Loading Notifications...',
    }).then(res => {
      res.present();
      this.soccerschoolService.notifications().subscribe(data => {
        this.notifications = data;
        res.dismiss();
      });
    });




  }
}
