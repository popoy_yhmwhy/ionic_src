import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeupClassPage } from './makeup-class.page';

describe('MakeupClassPage', () => {
  let component: MakeupClassPage;
  let fixture: ComponentFixture<MakeupClassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeupClassPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeupClassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
