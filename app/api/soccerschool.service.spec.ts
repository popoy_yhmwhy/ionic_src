import { TestBed } from '@angular/core/testing';

import { SoccerschoolService } from './soccerschool.service';

describe('SoccerschoolService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoccerschoolService = TestBed.get(SoccerschoolService);
    expect(service).toBeTruthy();
  });
});
