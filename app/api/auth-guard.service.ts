import { Injectable } from '@angular/core';
import { SoccerschoolService } from '../api/soccerschool.service';
import { CanActivate,Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate {

  constructor(
    public soccerschoolService : SoccerschoolService,
    public router: Router) { }
  
 
  
  canActivate(): boolean {
    const token  = localStorage.getItem('token'); 
    
    if(token){
      return true;
    }else{
      this.router.navigate(['login']);
      return false;
    }

  
  }

}
