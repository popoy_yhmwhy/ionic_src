import { Injectable } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SoccerschoolService {

  host = "http://espzen.com:82/soccerschool/api/";
  key = "";
  authState = new BehaviorSubject(false);
  user;

  constructor( private http : HttpClient) { }

  
  checkToken(){
    const params = new HttpParams().set('token', localStorage.getItem('token'));
    var url  = this.host + "account/token";
    return this.http.post(url,params,{
      headers: new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
    });
  }

  loaduserID(){
      var user = localStorage.getItem("user");
      var objUser = JSON.parse(user);
      return objUser.id;
  }

  upcomingSchedule(){
   var url  = this.host + "account/schedule/" + this.loaduserID();
    return this.http.get(url);
  }

  kids(){
    var url  = this.host + "account/kids/" + this.loaduserID();
    return this.http.get(url);
  }

  notifications(){
    var url  = this.host + "account/notifications/" + this.loaduserID();
    return this.http.get(url);
  }
 
  registrationDetail(id){
    var url  = this.host + "account/registration/" + id
    return this.http.get(url);
  }

  attending(attending,scheduleID){
    const params = new HttpParams().set('attending', attending);
    var url  = this.host + "account/attending/" + scheduleID;
    
    return this.http.post(url,params,{
      headers: new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
    });
  }

  login(username,password){
    
    const params = new HttpParams().set('username', username).set('password',password);
    var url  = this.host + "account/login";
    
    return this.http.post(url,params,{
      headers: new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
    });
    
  }
    


}
